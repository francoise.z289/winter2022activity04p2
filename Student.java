public class Student{
	private String name;
	private int age;
	private int stuId;
	private int amountLearnt;
	
	public void study(){
		System.out.println(this.name+"is studying...");
	}
	public void sayHi(){
		System.out.println(this.name+": Hi!");
	}
	public void learn(int amountStudied){
		if(amountStudied>0){
			this.amountLearnt=amountStudied;
		}
	}
	
	
	public String getName(){
		return this.name;
	}
	public int getAge(){
		return this.age;
	}
	public int getStuId(){
		return this.stuId;
	}
	public int getAmountLearnt(){
		return this.amountLearnt;
	}
	
	
	public void setAge(int newAge){
		this.age=newAge;
	}
	
	public Student(String name, int age){
		this.amountLearnt=108;
		this.name=name;
		this.age=age;
		this.stuId=0;
	}
	
}